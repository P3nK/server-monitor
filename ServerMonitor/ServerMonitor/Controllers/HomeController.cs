﻿using System;
using System.Threading;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace ServerMonitor.Controllers
{
    public class HomeController:Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AnotherLink()
        {
            return View("Index");
        }

        [HttpPost]
        public ActionResult SendEmail()
        {
            var secrets = new ClientSecrets()
            {
                ClientId = Environment.GetEnvironmentVariable("GmailClientId"),
                ClientSecret = Environment.GetEnvironmentVariable("GMailClientSecret")
            };

            var googleCredentials = GoogleWebAuthorizationBroker.AuthorizeAsync(secrets,new[] { GmailService.Scope.MailGoogleCom },"",CancellationToken.None);

            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Test From c# P3nK","pharit85@gmail.com"));
            message.To.Add(new MailboxAddress("To Programmer isc","programmer.isc@gmail.com"));
            message.Subject = "Hi, this is from P3nK c#";
            message.Body = new TextPart("plain") { Text = @"Test body message from c#" };
            using(var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s,c,h,e) => true;

                client.Connect("smtp.gmail.com",587,SecureSocketOptions.StartTlsWhenAvailable);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("pharit85@gmail.com","P3nk3517");
                client.Send(message);
                client.Disconnect(true);
            }
            return Json(true,JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System.Web.Mvc;

namespace ServerMonitor.Controllers
{
    public class ErrorController:Controller
    {
        [HttpGet]
        public ActionResult InternalServerError()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NotFound()
        {
            return View();
        }
    }
}
